<?php
require_once('phpChart_Lite/conf.php');
require_once("sql.php");

$title="Bubble Chart";
?>
<!DOCTYPE HTML>
<html>
    <head>
		<title><?php $title; ?></title>
    </head>
    <body>

<?php

$query="SELECT hour(rooms.timestamp) as 'Hour'
	,weekday(rooms.timestamp) as 'Day' 
	,count(rooms.id) as 'count'
	,room_name.name
	FROM rooms 
	left join room_name on room_id=roomid 
	where `occupied`=1 
	group by Day,Hour,room_id;";
$result=($dbh->query($query));
/*foreach($dbh->query($query) as $row) {
     
       $hour=$row['Hour'];
       $day=$row['Day'];
       $count=$row['count']; 
       $name=$row['name'];

            //echo '<pre>'.print_r($row,true).'</pre>';
    }*/

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Chart 1 Example
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
    $pc = new C_PhpChartX(array($result),'chart1');
    $pc->add_plugins(array('bubbleRenderer'));

    $pc->sort_data(true);
    $pc->set_title(array('text'=>'Bubble Chart'));
    $pc->set_series_default(array('renderer'=>'plugin::BubbleRenderer'
    	,'rendererOptions'=>array('autoscalePointsFactor'=>-.15
    		,'bubbleAlpha'=>0.6,'highlightAlpha'=>0.8)
    	,'highlightMouseDown'=>true,'shadow'=>true,'shadowAlpha'=>0.05));
    $pc->add_series(array('breakOnNull'=>true));
    $pc->set_axes(array(
        'xaxis'=>array('min'=>0,'max'=>7,'tickInterval'=>1),
        'yaxis'=>array('min'=>0, 'max'=>24,'tickInterval'=>1)
   ));

    $pc->draw(600,400);




    print_r($row);

?>

    </body>
</html>