<?php
	/****************************************************************
		Site Template file for smartThings Buildathon
		Written by: Timothy A Haas "HAASTA"
		Created: 2012-07-07
		Last Modified By: HAASTA | 2012-07-08 
	*****************************************************************/

	require('sql.php');
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style type="text/css">
		<!--
			html, body{
				top:0;
				bottom:0;
				height:100%; 
				width:100%; 
				margin:0px auto; 
				padding:0px; 
				text-align:center;
				font-size: 14px;
				color: #40392B;
				font-family: helvetica,arial,sans-serif, Verdana;
				/* IE10 Consumer Preview */ 
				background-image: -ms-linear-gradient(top, #f6f6f6 0%, #ccc 100%) fixed;
				/* Mozilla Firefox */ 
				background-image: -moz-linear-gradient(top, #f6f6f6 0%, #ccc 100%) fixed;
				/* Opera */ 
				background-image: -o-linear-gradient(top, #f6f6f6 0%, #ccc 100%) fixed;
				/* Webkit (Safari/Chrome 10) */ 
				background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #f6f6f6), color-stop(1, #ccc)) fixed;
				/* Webkit (Chrome 11+) */ 
				background-image: -webkit-linear-gradient(top, #f6f6f6 0%, #ccc 100%) fixed;
				/* W3C Markup, IE10 Release Preview */ 
				background-image: linear-gradient(to bottom, #f6f6f6 0%, #ccc 100%) fixed;
				background-color: #f6f6f6;
			}
			#smart_can_wrapper{ display: block; overflow: hidden;}
			#container_title{ font-size: 16px; font-weight: bold; color:#333; padding-top: 5px;}
			#smart_can_front{ display: block; float: left; overflow: hidden; margin: 10px; border: 1px solid #ccc; background: #efefef;}
			#smart_can_front_mens{ display: block; margin: 5px;}
			#smart_can_front_womens{ display: block; margin: 5px;}
			#smart_can_call_center{display: block; float: left; overflow: hidden; margin: 10px; border: 1px solid  #ccc; background: #efefef;}
			#smart_can_call_center_mens{ display: block; margin: 5px; }
			#smart_can_call_center_womens{ display: block; margin: 5px; }
			.smart_can{ float: left; height: 100px; width: 100px; border: 1px solid #ccc; background-color: white; font-size: 40px; color: #ccc;}
			.smart_can_disabled{ float: left; height: 130px; width: 130px; border: 1px solid #ccc; background-color: #f6f6f6; margin-bottom: 10px; font-size: 40px; color: #ccc;}
			.smart_can.occupied{ background-color: #CF7070; border-color: #941313; color: #f6f6f6;}
			.smart_can.available{ background-color: #72CE6F; border-color: #199413; color: #f6f6f6; }
		-->
		</style>
	</head>
	<body>
		<?php
			if($_GET[update]){ 
				$smartCanUpdateQuery ="UPDATE rooms SET occupied = $_GET[o] WHERE room_id = $_GET[id]";
				//print_r($smartCanUpdateQuery);
				$sth=$dbh->query($smartCanUpdateQuery);
				$sth->execute();
			}


			$smartCanStatusQuery = "SELECT DISTINCT r.room_id,
													r.occupied,
													r.timestamp,
													rn.name
											   FROM rooms r
									      LEFT JOIN room_name rn
									             ON rn.roomid = r.room_id
											  WHERE r.type = 'can'
										   GROUP BY r.room_id
											  HAVING MAX(r.timestamp)";
			$sth = $dbh->query($smartCanStatusQuery);
			$sth->execute();
			$smartCanStatusResults = $sth->fetchAll();
			//print_r($smartCanStatusResults);
			
		?>
		<div id="smart_can_wrapper"></div>
			<div id="smart_can_front">
				<div id="smart_can_front_mens">
					<div id="container_title">Front Men's Restroom</div>
					<div class="smart_can">M1</div>
					<div class="smart_can_disabled">DM2</div>
				</div>
				<div id="smart_can_front_womens">
					<div id="container_title">Front Women's Restroom</div>
					<div class="smart_can">W1</div>
					<div class="smart_can_disabled">DW1</div>
				</div>
			</div>
			<div id="smart_can_call_center">
				<div id="smart_can_call_center_mens">
					<div id="container_title">Call Center Men's Restroom</div>
					<div class="smart_can <?php if($smartCanStatusResults[0][room_id] == '5' && $smartCanStatusResults[0][occupied] == 1){ echo "occupied";}else{echo "available";}?>">M1</div>
					<div class="smart_can <?php if($smartCanStatusResults[1][room_id] == 8 && $smartCanStatusResults[1][occupied] == 1){ echo "occupied";}else{echo "available";}?>">M2</div>
					<div class="smart_can">M3</div>
					<div class="smart_can">M4</div>
					<div class="smart_can">M5</div>
					<div class="smart_can_disabled">DM6</div>
				</div>
				<div id="smart_can_call_center_womens">
					<div id="container_title">Call Center Women's Restroom</div>
					<div class="smart_can">W1</div>
					<div class="smart_can">W2</div>
					<div class="smart_can">W3</div>
					<div class="smart_can">W4</div>
					<div class="smart_can">W5</div>
					<div class="smart_can_disabled">DW6</div>
				</div>
			</div>
		
	</body>
</html>