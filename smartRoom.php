<?php
	require('sql.php');
$query="select r.timestamp,rn.name, r.type, CASE 'r.occupied'
WHEN 1 THEN 'YES'
WHEN 0 THEN 'NO'
END AS 'occupied' 
from rooms r
INNER JOIN room_name rn on rn.roomid = r.room_id
WHERE r.type = 'meeting'
GROUP BY rn.name,r.type
HAVING MAX(r.timestamp)
;
";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	</head>
	<body>
<?php
ECHO " <table border='2'> <tr><td>Room Name</td>
<td>Time</td>
<td>Occupied</td>
<td>Type</td>
</tr><br><br>";

foreach($dbh->query($query) as $row2) {
       echo "<tr><td>" ;
       echo $row2['name'];
       echo "</td>" ;
       echo "<td>" ;
       echo $row2['timestamp'];
       echo "</td>";
       echo "<td>" ;
       echo "<center><img src='media/";
       echo $row2['occupied'];
       echo ".jpg'></center>";
       echo "</td>" ;
       echo "<td>" ;
       echo $row2['type'];
       echo "</td></tr>" ;
       echo "<br>";
}

ECHO "</tr></table>";
//close connection
$dbh = null;
?>
	</body>
</html>