-- phpMyAdmin SQL Dump
-- version 3.4.11.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 15, 2013 at 09:13 AM
-- Server version: 5.5.30
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_smartThings`
--

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `occupied` varchar(1) NOT NULL,
  `type` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room_id`, `timestamp`, `occupied`, `type`) VALUES
(1, 1, '2013-03-14 16:55:25', '1', ''),
(2, 1, '2013-03-14 17:00:30', '0', ''),
(3, 1, '2013-03-14 17:02:51', '0', ''),
(4, 1, '2013-03-14 17:05:48', '0', ''),
(5, 4, '2013-03-14 22:39:00', '1', '0'),
(6, 4, '2013-03-14 22:42:22', '1', 'other'),
(7, 4, '2013-03-14 22:42:35', '1', 'other'),
(8, 5, '2013-03-14 22:43:15', '0', 'meeting'),
(9, 5, '2013-03-14 22:43:27', '1', 'meeting'),
(10, 5, '2013-03-14 22:44:33', '0', 'meeting'),
(11, 5, '2013-03-14 22:45:46', '0', 'can'),
(12, 5, '2013-03-14 22:45:48', '1', 'can'),
(13, 5, '2013-03-14 22:46:02', '0', 'can'),
(14, 5, '2013-03-14 22:46:03', '1', 'can'),
(15, 6, '2013-03-14 22:46:12', '1', 'other'),
(16, 5, '2013-03-14 22:46:12', '0', 'can'),
(17, 5, '2013-03-14 22:46:31', '1', 'can'),
(18, 6, '2013-03-14 22:46:39', '1', 'other'),
(19, 5, '2013-03-14 22:46:40', '0', 'can'),
(20, 5, '2013-03-14 22:46:50', '1', 'can'),
(21, 6, '2013-03-14 22:47:00', '1', 'other'),
(22, 5, '2013-03-14 22:47:02', '0', 'can'),
(23, 5, '2013-03-14 22:47:23', '1', 'can'),
(24, 6, '2013-03-14 22:47:47', '1', 'other'),
(25, 5, '2013-03-14 22:47:47', '0', 'can'),
(26, 5, '2013-03-14 22:47:58', '1', 'can'),
(27, 6, '2013-03-14 22:47:59', '1', 'other'),
(28, 5, '2013-03-14 22:47:59', '0', 'can'),
(29, 5, '2013-03-14 22:48:27', '1', 'can'),
(30, 6, '2013-03-14 22:48:39', '1', 'other'),
(31, 5, '2013-03-14 22:48:40', '0', 'can'),
(32, 5, '2013-03-14 22:48:46', '1', 'can'),
(33, 6, '2013-03-14 22:48:46', '1', 'other'),
(34, 5, '2013-03-14 22:48:47', '0', 'can'),
(35, 5, '2013-03-14 22:48:51', '1', 'can'),
(36, 6, '2013-03-14 22:48:57', '1', 'other'),
(37, 5, '2013-03-14 22:48:59', '1', 'can'),
(38, 5, '2013-03-14 22:49:00', '0', 'can'),
(39, 5, '2013-03-14 22:50:23', '1', 'meeting'),
(40, 5, '2013-03-14 22:53:00', '0', 'meeting');

-- --------------------------------------------------------

--
-- Table structure for table `room_name`
--

DROP TABLE IF EXISTS `room_name`;
CREATE TABLE IF NOT EXISTS `room_name` (
  `roomid` int(3) NOT NULL AUTO_INCREMENT COMMENT 'id on the room table',
  `name` varchar(55) COLLATE utf8_unicode_ci NOT NULL COMMENT 'name of the room',
  PRIMARY KEY (`roomid`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `room_name`
--

INSERT INTO `room_name` (`roomid`, `name`) VALUES
(1, 'design/sodev'),
(2, 'test1'),
(3, 'design1'),
(4, 'design2'),
(5, 'null'),
(6, 'room_123');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
