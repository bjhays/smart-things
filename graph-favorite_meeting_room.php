<?php
require_once('phpChart_Lite/conf.php');
require_once("sql.php");

$title="Favorite Meeting Room";
?>
<!DOCTYPE HTML>
<html>
    <head>
		<title><?php $title; ?></title>
    </head>
    <body>
<?php

$query="SELECT room_name.*,rooms.*,count(id) as 'count' FROM `rooms` left join room_name on room_id=roomid where `occupied`=1 && `type`='meeting' group by room_id";

$line1 = array();
$ticks = array();

foreach($dbh->query($query) as $row) {
       $line1[] = $row['count'];
       $ticks[] = $row['name'];
       //echo '<pre>'.print_r($row,true).'</pre>';
    }



//$line1 = array(14, 3, 4, 3, 5, 2, 3, 7);
//$ticks = array('1', '2', '3', '4', '5', '6', '7', '8', '9');

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Waterfall 1 Example
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$pc = new C_PhpChartX(array($line1),'chart1');
// $pc->add_plugins(array('barRenderer', 'categoryAxisRenderer', 'canvasAxisTickRenderer'));
$pc->add_plugins(array('logAxisRenderer','canvasTextRenderer','canvasAxisLabelRenderer','canvasAxisTickRenderer','dateAxisRenderer','categoryAxisRenderer','barRenderer'));

$pc->set_title(array('text' => $title));
$pc->set_series_default(array(
	'renderer'=>'plugin::BarRenderer',
	'rendererOptions'=>array(
				//'waterfall'=>true,
				'varyBarColor'=>true),
	'pointLabels'=>array('hideZeros'=>true),
	'yaxis'=>'y2axis'));		
$pc->set_xaxes(array(
	'xaxis'=>array(
				'renderer'=>'plugin::CategoryAxisRenderer',
				'ticks'=>$ticks,
				'tickRenderer'=>'plugin::CanvasAxisTickRenderer',
				'tickOptions'=>array(
					'angle'=> -90,
					'fontSize'=>'10pt',
					'showMark' => false,
					'showGridline' => false))));
$pc->set_yaxes(array(
	'y2axis'=>array(
				'tickInterval' => 5, 'min' => 0.1)));

$pc->draw(350,350);

/*
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Waterfall 2 Example
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$pc = new C_PhpChartX(array($line1),'chart2');
$pc->add_plugins(array('logAxisRenderer','canvasTextRenderer','canvasAxisLabelRenderer','canvasAxisTickRenderer','dateAxisRenderer','categoryAxisRenderer','barRenderer'));

$pc->set_title(array('text' => 'Crop Yield Charge, 2008 to 2009'));
$pc->set_series_color(array('#333333', '#999999', '#3EA140', '#3EA140', '#3EA140', '#783F16', '#783F16', '#783F16', '#333333'));
$pc->set_series_default(array(
	'renderer'=>'plugin::BarRenderer',
	'rendererOptions'=>array(
				'waterfall'=>true,
				'varyBarColor'=>true),
			'pointLabels'=>array('hideZeros'=>true),
			'yaxis'=>'y2axis'));

$pc->set_xaxes(array(
	'xaxis'=>array(
				'renderer'=>'plugin::CategoryAxisRenderer',
				'ticks'=>$ticks,
				'tickRenderer'=>'plugin::CanvasAxisTickRenderer',
				'tickOptions'=>array(
					'angle'=> -90,
					'fontSize'=>'10pt',
					'showMark' => false,
					'showGridline' => false))));
$pc->set_yaxes(array(
	'y2axis'=>array(
				'tickInterval' => 5, 'min' => 0.1)));

$pc->draw(350,350);
*/
?>

    </body>
</html>